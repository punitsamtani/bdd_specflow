﻿using SpecNuts.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace QTM_Specflow_POC
{
    [Binding]
    public sealed class Hooks
    {
        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            SpecNuts.Reporters.Add(new JsonReporter());
            SpecNuts.Reporters.FinishedReport += (sender, args) =>
            {
                String pathName = "specflow_cucumber.json";
                System.IO.File.WriteAllText(pathName, args.Reporter.WriteToString());
                Console.WriteLine("Result File: " +
                System.IO.Directory.GetCurrentDirectory().ToString() +
                System.IO.Path.DirectorySeparatorChar + pathName);
            };
        }
    }
}
