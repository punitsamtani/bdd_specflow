﻿using SpecNuts.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace QTM_Specflow_POC.StepDefiniations
{
    [Binding]
    class CalculatorFeatureSteps
    {
        
        [Given(@"I have entered (.*) into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator(int number)
        {
            Console.WriteLine("Entered number : " + number);
        }

        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            Console.WriteLine("Pressed Add Button");
        }

        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int result)
        {
            if (result == 120) // Grab tha object which has value 120 in the UI of your application and replace that
                Console.WriteLine("The Test PASSED");
            else
            {
                Console.WriteLine("The Test FAILED");
                throw new Exception("the value is different");
            }
        }

    }
}
